import { Controller, Get, Post, Delete, Patch, Body, Param, Query, UsePipes, ValidationPipe, ParseIntPipe, UseGuards } from '@nestjs/common';
import { TasksService } from './tasks.service';
import { CreateTaskDto } from './dto/create-task.dto';
import { GetTasksFilterDto } from './dto/get-tasks-filter.dto';
import { TaskStatusValidationPipe } from './pipes/pipes.status.validation.pipe';
import { Task } from './task.entity';
import { TaskStatus } from './task-status.enum';
import { AuthGuard, PassportModule } from '@nestjs/passport';
import { GetUser } from '../auth/get-user.decorator';
import { User } from '../auth/user.entity';

@Controller('tasks')
@UseGuards( AuthGuard())
export class TasksController {
    // inject task service into TaskController
    constructor(private tasksService: TasksService) {

    }

    // decorator ensures handler is called by NestJS when
    // get request with 'tasks' path is sent to server
    // when using ValidationPipe nestJS uses applies validation functions in DTO to validate request parameters
    @Get()
    getTasks(
        @Query(ValidationPipe)  filterDto: GetTasksFilterDto,
        @GetUser() user: User,
       ): Promise<Task[]> {
        // console.log('query= ', filterDto);

        return this.tasksService.getTasks( filterDto, user );

    }

    @Get('/:id')
    getTaskById(
        @Param('id', ParseIntPipe) id: number,
        @GetUser() user: User,
        ): Promise<Task> {
        // extract id from url and store in id parameter
        return this.tasksService.getTaskById(id, user);
    }

    @Post()
    @UsePipes(ValidationPipe)
    createTask(
        @Body() createTaskDto: CreateTaskDto,
        @GetUser() user: User,
    ): Promise<Task> {

    //   console.log('DTO =', createTaskDto);

      return  this.tasksService.createTask(createTaskDto, user);
    }

    @Delete('/:id')
    deleteTask(
        @Param('id', ParseIntPipe) id: number,
        @GetUser() user: User,
         ): Promise<void> {

       return this.tasksService.deleteTask(id, user);
    }

    @Patch('/:id/status')
    updateTaskStatus(@Param('id', ParseIntPipe) id: number,
    // wire up custom pipe to request Body
                     @Body('status', TaskStatusValidationPipe) status: TaskStatus,
                     @GetUser() user: User,
                      ): Promise<Task> {
        // console.log('id= ', id);
        // console.log('Body= ', status);
        // console.log('Status=', @Param('id'));
        return this.tasksService.updateTaskStatus(id, status, user);
    }
}
