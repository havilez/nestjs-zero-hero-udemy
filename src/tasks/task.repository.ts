import { Repository, EntityRepository } from 'typeorm';
import { Task } from './task.entity';
import { TaskStatus } from './task-status.enum';
import { CreateTaskDto } from './dto/create-task.dto';
import { GetTasksFilterDto } from './dto/get-tasks-filter.dto';
import { User } from '../auth/user.entity';

@EntityRepository(Task)
export class TaskRepository extends Repository<Task> {

    async getTasks( filterDto: GetTasksFilterDto, user: User ): Promise<Task[]> {
        const { status, search } = filterDto;

        // create query bulder which interacts with task tablei
        const query = this.createQueryBuilder('task');

        query.where('task.userId = :userId', { userId: user.id });

        if (status) {
            query.andWhere('task.status = :status', {status});
        }

        if (search) {
            query.andWhere('(task.title LIKE :search or task.description LIKE :search)', {search: `%${search}%` });
        }

        // apply query to DB
        const tasks = await query.getMany();
        return tasks;
    }

    async createTask(createTaskDTO: CreateTaskDto,
                     user: User): Promise<Task> {
        // destruct DTO object to assign fields from DTO to task Object
        const { title, description } = createTaskDTO;
        const task = new Task();
        task.title = title;
        task.description = description;
        task.status = TaskStatus.OPEN;
        task.user = user;
        await task.save();

        // delete user in results to hide confidential info
        delete task.user;

        return task;
    }

}
