FROM node:10.16.3-jessie

# * Alpine linux based images 
# * Need pkgs
#     * Build-base
#     * python
# https://github.com/kelektiv/node.bcrypt.js/wiki/Installation-Instructions
# RUN apk --no-cache add --virtual builds-deps build-base python

ENV NODE_ENV=development

EXPOSE 3000
# add tini parent process for clean shutdown
# RUN apk add --no-cache  tini


# see solution 2,for bind-mounts, when developing using docker-compose
# install node modules in parent directory of source code
RUN npm update && \
  npm i -g typescript ts-node nodemon 

#  removed because causes crash of build
#   npm cache clean

#  install node_modules in parent directory of the app
# allows for using node_modules in container , while still allowing for
#  node_modules to be built on the host, to be used for host debugging
WORKDIR /node

COPY package*.json  ./

RUN npm install && \
    # npm rebuild bcrypt --build-from-source && \
    npm cache clean --force 

ENV PATH /node/node_modules/.bin/:$PATH

# working directory for source code in sub-directory
WORKDIR /node/app

COPY . .

# Run parent tini process
# ENTRYPOINT ["/sbin/tini", "--"]

CMD [ "npm", "run", "start:dev"]

